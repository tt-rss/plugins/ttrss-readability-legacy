# af\_readability for PHP 5.6

This uses an older version of php-readability library which is no longer updated. Use if
you can't upgrade to PHP 7.0 (you really should though).

## Installation

Git clone to ``plugins.local/af_readability_legacy``
